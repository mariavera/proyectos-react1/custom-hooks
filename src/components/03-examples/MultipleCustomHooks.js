import React from 'react'
import { useCounter } from '../../hooks/useCounter'
import { useFetch } from '../../hooks/useFetch'
import '../02-useEffect/effects.css'
export const MultipleCustomHooks = () => {
    const { state, increment } = useCounter(1);
    console.log(state)
    const { loading, data } = useFetch(`https://www.breakingbadapi.com/api/quotes/${state}`)

    const { author, quote } = !!data && data[0];
    console.log(loading)
    return (
        <div>
            <h1>Breaking bad quotes</h1>
            <hr />
            {
                loading
                    ?
                    (
                        <div className='alert alert-info text-center'>
                            Loading...
                        </div>
                    )
                    : (
                        <blockquote className="blockquote text-right ">
                            <p className="mb-0"> {quote}
                            </p>
                            <footer className="blockquote-footer">
                                {author}</footer>
                        </blockquote>
                    )
            }

            <button className="btn btn-primary float-right" onClick={increment}>
                Next quote
</button>

        </div>
    )
}
