import React from 'react'
import { useCounter } from '../../hooks/useCounter'
import './counter.css'

export const CounterWithCustomHook = () => {
  const { state, increment, decrement, reset}  =useCounter();
    return (
        <>
            <h1>Counter with Hook: {state}</h1>
            <hr/>
            <div class="d-grid gap-2 d-md-block" >
            <button className="btn btn-outline-success"onClick= {()=>increment(2)} >+1</button>
            <button className="btn btn-outline-secondary"onClick= {reset} >Reset</button>
            <button className="btn btn-outline-danger" onClick= {()=>decrement(2)} disabled={state <= 0}>-1</button>
            </div>
        </>
    )
}
