import React, { useLayoutEffect, useRef } from 'react'
import { useCounter } from '../../hooks/useCounter'
import { useFetch } from '../../hooks/useFetch'
import './layout.css'
export const Layout = () => {
    const { state, increment } = useCounter(1);
    const { loading, data } = useFetch(`https://www.breakingbadapi.com/api/quotes/${state}`)

    const { quote } = !!data && data[0];
 const pTag =useRef();
    useLayoutEffect(
        () => {
console.log(pTag.current.getBoundingClientRect())
        }, [quote]
    )
    return (
        <div>
            <h1>Breaking bad quotes</h1>
            <hr />


            <blockquote className="blockquote text-right ">
                <p 
                ref={pTag}
                className="mb-0"> {quote}
                </p>
            </blockquote>



            <button className="btn btn-primary float-right" onClick={increment}>
                Next quote
</button>

        </div>
    )
}
