import React from 'react'
import { TodoListItem } from './TodoListItem'

export const TodoList = ({todos, handleDelete, handleToggle}) => {
    return (
      
                <ul className="list-group list-group-flush">
                    {todos.map((todo, i) => (
                        // TODOSLISTITEM, TODO, INDEX, HANDLEDELETE, HANDLETOGGLE
                       <TodoListItem todo={todo} i={i} handleDelete={handleDelete} handleToggle={handleToggle}/>
                    )

                    )}
                </ul>
    )
}
