import React from 'react'
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Redirect,
    
  } from "react-router-dom";
import { NavBar } from '../04-useRef/NavBar';
import { AboutScreen } from './AboutScreen';
import { HomeScreen } from './HomeScreen';
import { LoginScreen } from './LoginScreen';
export const AppRouter = () => {
    return (
       <Router>
           <>
           <NavBar/>
           <Switch>
               <Route exact path="/" component={ HomeScreen}/>
               <Route exact path="/about" component={ AboutScreen}/>
               <Route exact path="/login" component={ LoginScreen}/>
               <Route component={ HomeScreen}/>
           </Switch>
           </>
       </Router>
    )
}
