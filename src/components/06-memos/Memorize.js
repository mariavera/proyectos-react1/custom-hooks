import React, { useState } from 'react'
import { useCounter } from '../../hooks/useCounter'
import '../02-useEffect/effects.css'
import { Small } from './Small';
export const Memorize = () => {
    const [show, setShow] = useState(true)
    const {state, increment}= useCounter(10);
   console.log(state)
    return (
        <div>
            <h1>Counter: < Small value ={state}/> </h1>
            <hr/>

            <button 
            onClick={increment}
            className="btn btn-primary">
                +1
            </button>

            <button 
            onClick ={
                ()=> setShow(!show)
            }
            className="btn btn-outline-primary ml-3">Show/Hide {JSON.stringify( show)}</button>
        </div>
    )
}
