import React, { useState, useMemo } from 'react'
import { procesoPesado } from '../../helpers/procesoPesado'
import { useCounter } from '../../hooks/useCounter'
import '../02-useEffect/effects.css'
export const MemoHook = () => {
    const [show, setShow] = useState(true)
    const { state, increment } = useCounter(500);

  

    const memoProcesoPesado = useMemo( ()=> procesoPesado(state), [ state]);
    return (
        <div>
            <h1>MemoHook</h1>
            <h3>Counter: <small>{state}</small> </h3>
            <hr />
            <p>{memoProcesoPesado}</p>
            <button
                onClick={increment}
                className="btn btn-primary">
                +1
            </button>

            <button
                onClick={
                    () => setShow(!show)
                }
                className="btn btn-outline-primary ml-3">Show/Hide {JSON.stringify(show)}</button>
        </div>
    )
}
